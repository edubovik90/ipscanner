package dubovik.ua.ipsearch.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import dubovik.ua.ipsearch.tools.SubnetUtils;

public class AsyncScanner extends AsyncTask<Void, Void, Void> {
    Context context;
    String mask;
    Handler handler;
    SubnetUtils subnetUtils;
    String[] addresses;
    List<String> tmpList=new ArrayList<>();
    AsyncCallback callback;
    ProgressDialog dialog;

    public AsyncScanner(Context context, String mask, AsyncCallback callback) {
        this.context = context;
        this.mask = mask;
        this.callback = callback;

    }

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show(context, "Network Scanning", null, true);

    }

    @Override
    protected Void doInBackground(Void... params) {
        findIpAddress();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        dialog.dismiss();
    }

    private void findIpAddress() {
        try {
            WifiManager wifiMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            int wifiInfoIpAddress = wifiInfo.getIpAddress();
            String ipAddress = Formatter.formatIpAddress(wifiInfoIpAddress);
            int subnetMask = -1;

            try {
                if (mask.length() <= 2) {
                    subnetUtils = new SubnetUtils(ipAddress + "/" + mask);
                    subnetMask = Integer.valueOf(mask);
                } else {
                    subnetUtils = new SubnetUtils(ipAddress, mask);
                    subnetMask = subnetUtils.getInfo().getNetmaskInt();
                }
            } catch (IllegalArgumentException e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Incorrect value of mask", Toast.LENGTH_SHORT).show();
                    }
                });

            }
            Log.e("MASK", subnetMask+" >>> "+mask);
            byte[] lowIP = InetAddress.getByName(subnetUtils.getInfo().getLowAddress()).getAddress();
            byte[] highIP = InetAddress.getByName(subnetUtils.getInfo().getHighAddress()).getAddress();
            int[] range;

            if (subnetMask >= 24) {
                range = getAddressRange(lowIP, highIP, 3);
                typeOne(range[0], range[1], lowIP);
                addresses = tmpList.toArray(new String[tmpList.size()]);
            } else if (subnetMask >= 16 && subnetMask < 24) {
                range = getAddressRange(lowIP, highIP, 2);
                typeTwo(range[0], range[1], lowIP);
                addresses = tmpList.toArray(new String[tmpList.size()]);
            } else if (subnetMask >= 8 && subnetMask < 16) {
                range = getAddressRange(lowIP, highIP, 1);
                typeThree(range[0], range[1], lowIP);
                addresses = tmpList.toArray(new String[tmpList.size()]);
            } else if (subnetMask >= 0 && subnetMask < 8) {
                range = getAddressRange(lowIP, highIP, 0);
                typeFour(range[0], range[1], lowIP);
                addresses = tmpList.toArray(new String[tmpList.size()]);
            }
            callback.onFinish(addresses);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAddressInfo(InetAddress address) {
        tmpList.add(address.getHostAddress());
        Log.e("FINDER", "Using InetAddress");
        Log.e("FINDER", "Host Address: " + address.getHostAddress());
        Log.e("FINDER", "Host Name: " + address.getHostName());

    }


    private void typeOne(int lowValue, int highValue, byte[] lowAddress) throws IOException {
        InetAddress ip;

        do {
            lowAddress[3] = (byte) lowValue;
            ip = InetAddress.getByAddress(lowAddress);
            if (ip.isReachable(70)) {
                getAddressInfo(ip);
            }
            lowValue++;
        } while (lowValue <= highValue);
    }

    private void typeTwo(int lowValue, int highValue, byte[] lowAddress) throws IOException {
        InetAddress ip;
        int count4 = 1;
        do {

            do {
                if (lowValue == highValue && count4 == 255) {
                    continue;
                }
                lowAddress[3] = (byte) count4;
                ip = InetAddress.getByAddress(lowAddress);
                if (ip.isReachable(70)) {
                    getAddressInfo(ip);
                }
                count4++;
            } while (count4 <= 255);
            count4 = 0;
            lowValue++;
            lowAddress[2] = (byte) lowValue;
        } while (lowValue <= highValue);
    }

    private void typeThree(int lowValue, int highValue, byte[] lowAddress) throws IOException {
        InetAddress ip;
        int count3 = 0;
        int count4 = 0;
        do {
            do {

                do {
                    if (lowValue == highValue && count3 == 255 && count4 == 255) {
                        continue;
                    }
                    lowAddress[3] = (byte) count4;
                    ip = InetAddress.getByAddress(lowAddress);
                    if (ip.isReachable(70)) {
                        getAddressInfo(ip);
                    }
                    count4++;
                } while (count4 <= 255);
                count4 = 0;
                count3++;
                lowAddress[2] = (byte) count3;
            } while (count3 <= 255);
            count3 = 0;
            lowValue++;
        } while (lowValue <= highValue);
    }

    private void typeFour(int lowValue, int highValue, byte[] lowAddress) throws IOException {
        InetAddress ip;
        int count2 = 0;
        int count3 = 0;
        int count4 = 1;
        do {
            do {
                do {

                    do {
                        if (lowValue == highValue && count2 == 255 && count3 == 255 && count4 == 255) {
                            continue;
                        }
                        lowAddress[3] = (byte) count4;
                        ip = InetAddress.getByAddress(lowAddress);
                        if (ip.isReachable(70)) {
                            getAddressInfo(ip);
                        }
                        count4++;
                    } while (count4 <= 255);
                    count4 = 0;
                    count3++;
                    lowAddress[2] = (byte) count3;
                } while (count3 <= 255);
                count3 = 0;
                count2++;
                lowAddress[1] = (byte) count2;
            } while (count2 <= 255);
            count2 = 0;
            lowValue++;
            lowAddress[0] = (byte) lowValue;
        } while (lowValue <= highValue);
    }

    private int[] getAddressRange(byte[] low, byte[] high, int position) {
        int[] result = {
                low[position] & 0xFF,
                high[position] & 0xFF};
        return result;
    }


}
