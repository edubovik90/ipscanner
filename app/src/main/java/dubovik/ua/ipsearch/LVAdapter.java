package dubovik.ua.ipsearch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LVAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final String[] values;

    public LVAdapter(Context context, String[] values) {
        super(context, R.layout.row_info, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_info, parent, false);
        TextView number = (TextView) rowView.findViewById(R.id.number);
        TextView address = (TextView) rowView.findViewById(R.id.ip_address);

        number.setText("#" + (position+1) + ":");
        address.setText("IP: " + values[position]);


        return rowView;
    }
}
