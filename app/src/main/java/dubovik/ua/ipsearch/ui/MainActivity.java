package dubovik.ua.ipsearch.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import dubovik.ua.ipsearch.LVAdapter;
import dubovik.ua.ipsearch.R;
import dubovik.ua.ipsearch.network.AsyncCallback;
import dubovik.ua.ipsearch.network.AsyncScanner;


public class MainActivity extends ActionBarActivity {
    Handler handler;
    ListView lv;
    LVAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText maskET = (EditText) findViewById(R.id.mask_tv);
        lv = (ListView) findViewById(R.id.ip_lv);
        handler = new Handler();

        Button start = (Button) findViewById(R.id.start);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mask = maskET.getText().toString();
                if (!mask.isEmpty()) {
                    new AsyncScanner(MainActivity.this, mask, new AsyncCallback() {
                        @Override
                        public void onFinish(final String[] array) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    adapter = new LVAdapter(MainActivity.this, array);
                                    lv.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                }
                            });
                        }
                    }).execute();
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

}
